# Продажа, возврат билета и закрытие касс администратором. Интерфейс – консольный.

class Client:
    def __init__(self):
        self.pasport = True # Паспорт клиента
        self.cash = int(450) # Наличные деньги
        self.city_ticket = False # Имеет билет в город
        self.village_ticket = False # Имеет билет в поселок

    def buy_ticket (self, a):
        if a == True:
            self.cash -= 250
        else:
            self.cash -= 200            

    def moneyback (self, c):
        self.pasport = False
        self.cash += c

class Administrator:
    def __init__ (self):
        pass

    def cashbox_close (self):
        cb.working = False

class Cashbox:
    def __init__ (self):
        self.cash = 0
        self.pasport = False
        self.working = True

    def end (self):
        self.pasport = False

    def close (self):
        pass

cl = Client()
am = Administrator()
cb = Cashbox()

change = input('1 – Купить билет\n2 – Вернуть билет\n3 – Данные клиента\n4 – Закрыть кассу (администратор)\n')
if change == '1' and cb.working == True:
    change_buy = input('Купить билет в:\n1 – Город\n2 – Поселок\n')
    if change_buy == '1':
        cl.city_ticket = True
        cl.buy_ticket(True)
        print('Наличные: ', cl.cash, 'Паспорт: ', cl.pasport, 'Билет в город: ', cl.city_ticket, 'Билет в поселок: ', cl.village_ticket )
    elif change_buy == '2':
        cl.village_ticket = True
        cl.buy_ticket(False)
        print('Наличные: ', cl.cash, 'Паспорт: ', cl.pasport, 'Билет в город: ', cl.city_ticket, 'Билет в поселок: ', cl.village_ticket )
    else:
        pass
elif change == '2' and cb.working == True:
    if cl.city_ticket == True:
        cb.pasport = True
        cb.city_ticket = False
        cl.moneyback(250)
        cb.cash -= 250
        cb.end()
        print('Наличные: ', cl.cash, 'Паспорт: ', cl.pasport, 'Билет в город: ', cl.city_ticket, 'Билет в поселок: ', cl.village_ticket )
    elif cl.village_ticket == True:
        cb.pasport = True
        cl.village_ticket = False
        cl.moneyback(200)
        cb.cash -= 200
        cb.end()
        print('Наличные: ', cl.cash, 'Паспорт: ', cl.pasport, 'Билет в город: ', cl.city_ticket, 'Билет в поселок: ', cl.village_ticket )
    else:
        pass
elif change == '3':
    print('Наличные: ', cl.cash, 'Паспорт: ', cl.pasport)
elif change == '4' and cb.working == True:
    am.cashbox_close()
else:
    print('Неправильные входные данные.')